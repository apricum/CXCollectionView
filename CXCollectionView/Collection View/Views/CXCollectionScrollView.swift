//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 18/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// A view that displays a portion of an enclosed `CXCollectionView` to satisfy the AppKit requirements of
/// every collection view needing a scroll view as wrapper. Supports animated resizing operations already
/// prepared and executed by the collection view itself based on presented content.
open class CXCollectionScrollView: NSScrollView, CXCollectionEnclosure {
	
	public typealias CompletionHandler = () -> Void
	
	// MARK: Reference Properties
	
	public var enclosedViewHeightConstraint: NSLayoutConstraint?
	
	// MARK: State Properties
	
	public var preparedEnclosedViewHeightChange: (original: CGFloat, updated: CGFloat)?
	
	// MARK: Configuration Properties
	
	/// The priority assigned to the height constraint of the scroll view, with its constant updated on reload
	/// and set to the intrinsic content size of the collection view and its contents.
	open var heightConstraintPriority: NSLayoutConstraint.Priority { .defaultHigh }
	
	/// The duration of any resizing animations run on the scroll view.
	open var animationDuration: TimeInterval { 0.3 }
	
	private var drawsDebugBackground: Bool = false
	private var debugBackgroundColor = NSColor(hue: 0.7, saturation: 0.25, brightness: 0.75, alpha: 1)
	
	// MARK: Sourced Properties
	
	public var enclosedViewHeight: CGFloat { CXCollectionView.clippedVisibleBoundsHeight(enclosedSizableView?.estimatedViewSize.height ?? 0) }
	public var enclosedViewAppliedHeight: CGFloat { enclosedViewHeightConstraint?.constant ?? 0 }
	
	private var enclosedSizableView: CXSizableView? { documentView as? CXSizableView }
	
	// MARK: Init
	
	public override init(frame frameRect: NSRect) {
		super.init(frame: frameRect)
		commonInit()
	}
	
	public required init?(coder: NSCoder) {
		super.init(coder: coder)
		commonInit()
	}
	
	open func commonInit() {
		wantsLayer = true
		setUpConstraints()
	}
	
	private func setUpConstraints() {
		let heightConstraint = heightAnchor.constraint(equalToConstant: enclosedViewHeight)
		heightConstraint.priority = heightConstraintPriority
		NSLayoutConstraint.activate([heightConstraint])
		
		self.enclosedViewHeightConstraint = heightConstraint
	}
	
	// MARK: Drawing
	
	override open func draw(_ dirtyRect: NSRect) {
		super.draw(dirtyRect)
		
		if drawsDebugBackground {
			debugBackgroundColor.setFill()
			bounds.fill()
		}
	}
	
	// MARK: Layout Lifecycle
	
	open func resizeEnclosure(withAnimation shouldAnimate: Bool) {
		resizeEnclosure(withAnimation: shouldAnimate, nil)
	}
	
	open func resizeEnclosure(withAnimation shouldAnimate: Bool, _ completionHandler: CompletionHandler?) {
		guard needsEnclosureResize else {
			completionHandler?()
			return
		}
		
		guard let heightConstraint = enclosedViewHeightConstraint else {
			print("Missing height constraint while updating sourced constraints in scroll view. Missing set-up.")
			completionHandler?()
			return
		}
		
		guard shouldAnimate else {
			applyEnclosureSize(to: heightConstraint)
			completionHandler?()
			return
		}
		
		NSAnimationContext.runAnimationGroup({ context in
			context.duration = animationDuration
			context.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
			
			applyEnclosureSize(to: heightConstraint.animator())
		}, completionHandler: completionHandler)
	}
	
	// MARK: Constraint Update
	
	private func applyEnclosureSize(to constraint: NSLayoutConstraint) {
		if preparedEnclosedViewHeightChange == nil {
			prepareForEnclosureResize()
		}
		
		let updatedHeight = preparedEnclosedViewHeightChange!.updated
		preparedEnclosedViewHeightChange = nil
		
		constraint.constant = updatedHeight
	}
    
}
