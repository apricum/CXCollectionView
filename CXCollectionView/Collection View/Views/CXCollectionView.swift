//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 11/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// An ordered collection of data items displayed in a single-column layout
/// with convenience functionality for reloading and animating the difference on
/// content changes, as well as cooperating with an enclosing scroll view to size
/// itself based on presentable data.
open class CXCollectionView: NSCollectionView, CXCollectionViewForm, CXCollectionViewContentSizeProvider, CXTraversableCollectionView {
	
	// MARK: References
	
	open weak var contentDataSource: CXCollectionViewDataSource?
	open weak var itemForm: CXCollectionViewItemForm?
	
	private(set) var dataSourceAdapter: CXCollectionViewDataSourceAdapter!
	
	// MARK: State
	
	private(set) open var needsContentUpdate: Bool = false
	
	private(set) open var preparedContentUpdateData: CollectionUpdateData?
	private(set) open var preparedCollectionViewHeight: CGFloat = CXCollectionView.noEstimatedMetric
	
	// MARK: Properties
	
	/// Returns the estimated, previously prepared and cached height of the
	/// collection view. Can be updated by calling `invalidateCollectionViewHeight()`
	/// to synchronize with currently visualizable data.
	open var collectionViewHeight: CGFloat { preparedCollectionViewHeight }
	
	/// Returns the estimated height the collection view's content would occupy to
	/// be displayed in full. Similar to the `intrinsicContentSize` property, returns
	/// `noEstimatedMetric` if size can not be determined.
	///
	/// This property serves as passthrough to sizing logic adopted by
	/// `CXSizableCollectionView` and can be overridden in subclasses.
	///
	/// Property is never cached and is only used for recalculation after size invalidation.
	/// Use cached `collectionViewHeight` property for common access instead.
	open var estimatedCollectionViewHeight: CGFloat { estimatedContentHeight(self) ?? Self.noEstimatedMetric }

	// MARK: Debug

	open var debugIdentifier: String?
	
	// MARK: Init
	
	override open func awakeFromNib() {
		super.awakeFromNib()
		
		let layout = NSCollectionViewFlowLayout()
		layout.minimumLineSpacing = 0
		layout.minimumInteritemSpacing = 0
		self.collectionViewLayout = layout
		
		let adapter = CXCollectionViewDataSourceAdapter()
		self.dataSourceAdapter = adapter
		self.dataSource = adapter
	}
	
	/// Reloads all dependencies used in visualization by the collection view.
	/// Primary use is to set up collection view items, section headers and
	/// auxiliary items.
	open func reloadStructure() {
		dataSourceAdapter.registerViews(self)
	}
	
	/// Determines a new estimated view height by available content and stores
	/// it internally.
	open func invalidateCollectionViewHeight() {
		preparedCollectionViewHeight = estimatedCollectionViewHeight
	}
	
	// MARK: Validation
	
	private func assertSource() {
		assert(dataSource is CXCollectionViewDataSourceAdapter, "Collection view is missing a valid data source adapter.")
	}
	
	// MARK: Sourcing
	
	/// Sources all visualizable contents as a sequence of modeled sections from the
	/// referenced data source chain.
	private var sourcedContent: [Section] {
		assertSource()
		
		var sections: [Section] = []
		let numberOfSections = contentDataSource?.collectionViewNumberOfSections(self) ?? 0
		
		for sectionIndex in 0 ..< numberOfSections {
			guard let section = contentDataSource?.collectionView(self, sectionAt: sectionIndex) else {
				fatalError("Content data source must return section for index \(sectionIndex), previously indicated \(numberOfSections) section(s).")
			}
			
			sections.append(section)
		}
		
		return sections
	}
    
}

// MARK: Statics

extension CXCollectionView {
	
	/// The minimum height needed for the collection view and its enclosing
	/// scroll and clip view for it to load and display items.
	///
	/// Required as a minimum value for layout constraints for the collection
	/// view to insert items with animations. With zero bounds, the view
	/// does not draw any contents.
	@inlinable public static var minimumVisibleBoundsHeight: CGFloat { 1 }
	
	/// Returns bounds height that satisfy the minimum requirements for the
	/// collection view to draw items. Limits the value to be higher
	/// than `minimumVisibleBoundsHeight`.
	@inlinable public static func clippedVisibleBoundsHeight(_ proposedHeight: CGFloat) -> CGFloat {
		return max(Self.minimumVisibleBoundsHeight, proposedHeight)
	}
	
}

// MARK: Content Access

extension CXCollectionView {
	
	/// Returns if the internal content cache does not contain any objects.
	open var hasEmptyContentCache: Bool { dataSourceAdapter.hasEmptyContentCache }
	
	/// Returns the number of content cached sections.
	open var cachedNumberOfSections: Int {
		dataSourceAdapter.cachedNumberOfSections
	}
	
	/// Returns the content cached section at the given index.
	open func cachedSection(at section: Int) -> Section {
		dataSourceAdapter.cachedSection(at: section)
	}
	
	/// Returns the content cached object at the given index path.
	open func cachedObject(at indexPath: IndexPath) -> Object {
		dataSourceAdapter.cachedObject(at: indexPath)
	}
	
}

// MARK: Sizable View

extension CXCollectionView: CXSizableView {
	
	public var estimatedViewSize: CGSize { CGSize(width: -1, height: collectionViewHeight) }
	
}

// MARK: Change Controller

extension CXCollectionView: CXCollectionViewChangeController {
	
	// MARK: Lifecycle
	
	/// Computes the difference between the originally processed content and new sourced content
	/// and stores it. Determined changes are kept internally at `preparedContentUpdateData`
	/// and the collection view is marked to need a content update in a future cycle.
	@objc open func prepareContentUpdate() {
		// Store updated content in data source adapter, store difference for processing.
		// If another content difference still exists, use least recent for differentiation.
		let (originalContent, updatedContent) = dataSourceAdapter.overwriteContent(sourcedContent)
		let contentUpdateData = (preparedContentUpdateData?.original ?? originalContent, updatedContent)
		
		guard hasChanges(in: contentUpdateData) else {
			return
		}
		
		preparedContentUpdateData = contentUpdateData
		needsContentUpdate = true
	}
	
	/// Performs a content update in the collection view and its modules is the
	/// view was marked to need one ahead of time.
	@objc open func updateContentIfNeeded(_ completionHandler: CompletionHandler? = nil) {
		// Execute content updates with stored prepared collections if set.
		if needsContentUpdate, let preparedContentDifference = preparedContentUpdateData {
			willUpdateContent(withAnimation: true) {
				self.applyUpdate(self, in: preparedContentDifference, completionHandler)
				self.discardPreparedContentUpdate()
			}
		} else {
			completionHandler?()
			discardPreparedContentUpdate()
		}
	}

	/// Performs a hard content reload through the built-in `reloadData()` of  `NSCollectionView`.
	///
	/// Function is only used as assisting functionality to other content update functions.
	@objc open func reloadContentIfNeeded() {
		// Run content reload without animation or batch performance.
		guard needsContentUpdate else {
			discardPreparedContentUpdate()
			return
		}

		willUpdateContent(withAnimation: false) {
			self.reloadData()
			self.discardPreparedContentUpdate()
		}
	}
	
	/// Removes the internally kept content difference and removes resets the need
	/// of the collection view needing a content update.
	@objc open func discardPreparedContentUpdate() {
		preparedContentUpdateData = nil
		needsContentUpdate = false
	}
	
	/// Lifecycle function called before a content update will be performed.
	///
	/// Can be used to delay the actual execution of the content update, for
	/// instance, to run preparation tasks or handle other controls or views
	/// added to the collection view through a subclass or enclosures.
	///
	/// - Important: The provided closure `continueContentUpdate` *must
	/// be called* at some point during or after the execution of this function.
	/// In a subclass, *super* can be called instead.
	@objc open func willUpdateContent(withAnimation shouldAnimate: Bool, _ continueContentUpdate: @escaping CompletionHandler) {
		continueContentUpdate()
	}
	
	/// Lifecycle function called once a content update has been completed,
	/// including all animations.
	@objc open func didUpdateContent(withAnimation didAnimate: Bool) {}
	
	// MARK: Differentiation
	
	/// Determines if the given collection data change model has differences
	/// between its original and updated versions.
	private func hasChanges(in data: CollectionUpdateData) -> Bool {
		guard data.original.count == data.updated.count else {
			return true
		}
		
		for sectionIndex in data.original.indices {
			let (originalSection, updatedSection) = (data.original[sectionIndex], data.updated[sectionIndex])
			
			guard originalSection.sectionId == updatedSection.sectionId else {
				return true
			}
			
			guard originalSection.hasEqualCollectionObjects(as: updatedSection) else {
				return true
			}
		}
		
		return false
	}
	
}
