//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 11/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

class CXCollectionViewDataSourceAdapter: NSObject, CXCollectionViewForm {
	
	// MARK: State
	
	/// The cached content consisting of data sections, prepared from sourced data.
	private var cachedContent: [Section] = []
	
	// MARK: Init
	
	/// Asks the referenced item form delegate to register views used by the collection view.
	func registerViews(_ collectionView: NSCollectionView) {
		let collectionView = collectionView as! CXCollectionView
		collectionView.itemForm?.registerCollectionViewItems(collectionView)
	}
	
	// MARK: Content Access
	
	/// Overwrites internally cached existing content (sections and items) with the given updated
	/// content and returns a tuple of both the original and updated content data.
	func overwriteContent(_ updatedContent: [Section]) -> (original: [Section], updated: [Section]) {
		let originalContentIds = cachedContent
		overwriteInternalContent(updatedContent)
		
		let updatedContentIds = cachedContent
		return (originalContentIds, updatedContentIds)
	}
	
	/// Overwrites internally cached content with the given updated content.
	private func overwriteInternalContent(_ updatedContent: [Section]) {
		cachedContent = updatedContent
	}
	
}

// MARK: Data Source

extension CXCollectionViewDataSourceAdapter: NSCollectionViewDataSource {
	
	// MARK: Contents
	
	func numberOfSections(in collectionView: NSCollectionView) -> Int {
		return cachedNumberOfSections
	}
	
	func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
		guard section < cachedNumberOfSections else {
			// TODO: Transform into an assertion failure once superfluous calls have been fixed.
			// Number of sections never reports out of range but number of items is asked anyway.
			print("Collection view asked for number of items in out of bounds section \(section) in collection with \(cachedNumberOfSections) section(s).")
			return 0
		}
		
		return cachedContent[section].count
	}
	
	func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
		let collectionView = collectionView as! CXCollectionView
		let object = cachedObject(at: indexPath)
		
		guard let item = collectionView.itemForm?.collectionView(collectionView, object: object, at: indexPath) else {
			fatalError("Missing item form or item for item at represented object at \(indexPath).")
		}
		
		return item
	}
	
	func collectionView(_ collectionView: NSCollectionView, viewForSupplementaryElementOfKind kind: NSCollectionView.SupplementaryElementKind, at indexPath: IndexPath) -> NSView {
		let collectionView = collectionView as! CXCollectionView
		
		guard let view = collectionView.itemForm?.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath) else {
			fatalError("Missing item form or item for supplementary element of kind '\(kind)' for item at '\(indexPath)'.")
		}
		
		return view
	}
	
}

// MARK: Content Access

extension CXCollectionViewDataSourceAdapter {
	
	/// Returns if the internal content cache does not contain any objects.
	/// While the cache might contain sections, if all kept are empty, the
	/// content cache as a whole is still considered empty.
	var hasEmptyContentCache: Bool {
		for modeledSection in cachedContent {
			guard modeledSection.isEmpty == true else {
				return false
			}
		}
		
		return true
	}
	
	/// Returns the number of content cached sections.
	var cachedNumberOfSections: Int {
		return cachedContent.count
	}
	
	/// Returns the content cached modeled section at the given index.
	func cachedSection(at section: Int) -> Section {
		guard cachedContent.indices.contains(section) else {
			fatalError("Collection view data source adapter does not hold a section for enquired index \(section). Cache holds \(cachedContent.count) section(s).")
		}
		
		return cachedContent[section]
	}
	
	/// Returns the content cached object from its section at the given index path.
	func cachedObject(at indexPath: IndexPath) -> Object {
		let section = cachedSection(at: indexPath.section)
		guard section.objects.indices.contains(indexPath.item) else {
			fatalError("Collection view data source adapter does not hold an object at index \(indexPath.item) in section \(indexPath.section). Section in cache holds \(section.objects.count) object(s).")
		}
		
		return section[indexPath.item]
	}
	
}
