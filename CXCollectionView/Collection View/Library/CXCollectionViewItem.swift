//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 16/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// A visual representation for a single data element in a `CXCollectionView`.
public protocol CXCollectionViewItem: NSCollectionViewItem {}
