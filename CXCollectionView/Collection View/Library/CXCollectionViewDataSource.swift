//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 24/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// A set of methods that a data source object implements to provide information around presentable data for a `CXCollectionView`.
public protocol CXCollectionViewDataSource: AnyObject, CXCollectionViewForm {
	
	// MARK: Data
	
	func collectionViewNumberOfSections(_ collectionView: CXCollectionView) -> Int
	
	func collectionView(_ collectionView: CXCollectionView, sectionAt sectionIndex: Int) -> Section
	
}
