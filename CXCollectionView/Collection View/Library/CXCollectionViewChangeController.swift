//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 17/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa
import Differ

/// Functionality to execute changes on a collection view and its presented data.
///
/// Adopted by `CXCollectionView` itself, exposing extended functions for performing changes,
/// in turn calling through to existing logic of `NSCollectionView`.
public protocol CXCollectionViewChangeController: CXCollectionViewForm {}

extension CXCollectionViewChangeController {
	
	public typealias CompletionHandler = () -> Void
	public typealias CollectionUpdateData = (original: [Section], updated: [Section])
	public typealias CollectionDataDiff = NestedExtendedDiff
	
	// MARK: Changes
	
	/// Executes changes contained in the given differential collection in the supplied collection view.
	/// Completion handler is called if provided after the collection view's animations have finished.
	///
	/// Commonly not called directly but used indirectly through the change initiator protocol.
	/// When directly calling this function, data sources and internal caches need to be prepared ahead of time.
	func applyUpdate(_ collectionView: NSCollectionView, in data: CollectionUpdateData, _ completionHandler: CompletionHandler? = nil) {
		let collectionView = collectionView as! CXCollectionView
		
		collectionView.animateItemAndSectionChanges(
			oldData: data.original,
			newData: data.updated,
			isEqualSection: equateSections,
			isEqualElement: equateObjects,
			indexPathTransform: { indexPath in indexPath },
			sectionTransform: { section in section },
			completion: { _ in
				completionHandler?()
			}
		)
	}
	
	// MARK: Differentiation
	
	/// Returns the item difference from the given update data.
	func collectionDiff(in data: CollectionUpdateData) -> CollectionDataDiff {
		let (original, updated) = data
		return original.nestedExtendedDiff(to: updated, isEqualSection: equateSections, isEqualElement: equateObjects)
	}
	
	private func equateSections(_ lhs: Section, _ rhs: Section) -> Bool {
		return lhs.sectionId == rhs.sectionId
	}
	
	private func equateObjects(_ lhs: Object, _ rhs: Object) -> Bool {
		return lhs.objectId == rhs.objectId
	}
	
}
