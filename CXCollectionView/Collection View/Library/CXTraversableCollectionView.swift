//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 22/04/2020.
//  Copyright © 2020 Apricum Clb. All rights reserved.
//

import Foundation

/// A collection view that provides functionality to have its contents traversed like a sequence.
public protocol CXTraversableCollectionView: CXCollectionView {}

extension CXTraversableCollectionView {
	
	/// Returns all index paths possibly presentable by the collection view by the data currently available.
	public var indexPathsForAllItems: Set<IndexPath> {
		var indexPaths = Set<IndexPath>()
		let numberOfSections = dataSource?.numberOfSections?(in: self) ?? 0
		
		for section in 0 ..< numberOfSections {
			indexPaths.formUnion(indexPathsForAllItems(in: section))
		}
		
		return indexPaths
	}
	
	/// Returns all index paths possibly presentable by the collection view by the data currently available in the given section.
	public func indexPathsForAllItems(in section: Int) -> Set<IndexPath> {
		var indexPaths = Set<IndexPath>()
		let numberOfItems = dataSource?.collectionView(self, numberOfItemsInSection: section) ?? 0
		
		for item in 0 ..< numberOfItems {
			let indexPath = IndexPath(item: item, section: section)
			indexPaths.insert(indexPath)
		}
		
		return indexPaths
	}
	
	/// Iterates through all index paths presentable in the collection with data specified by its data source.
	/// If the collection view does not have a data source, no index paths will be iterated through.
	public func forEachIndexPath(_ block: (_ indexPath: IndexPath) -> Void) {
		for indexPath in indexPathsForAllItems {
			block(indexPath)
		}
	}
	
	/// Iterates through items for all index paths presentable in the collection with data specified by its data source.
	/// If the collection view does not have a data source, no index paths will be iterated through.
	public func forEachItem(_ block: (_ indexPath: IndexPath, _ item: CXCollectionViewItem) -> Void) {
		forEachIndexPath { indexPath in
			guard let item = self.item(at: indexPath) as? CXCollectionViewItem else {
				assertionFailure("Could not get excavated collection view item at index path '\(indexPath)', either does not exist or is lacking conformance to 'CXCollectionViewItem'.")
				return
			}
			
			block(indexPath, item)
		}
	}

	/// Iterates through all index paths that are currently presented by the collection view.
	///
	/// Note that, as with `indexPathsForVisibleItems` and `visibleItems`, the
	/// index paths and items returned are currently managed by the collection view
	/// but not necessarily visible.
	public func forEachVisibleIndexPath(_ block: (_ indexPath: IndexPath) -> Void) {
		for indexPath in indexPathsForVisibleItems() {
			block(indexPath)
		}
	}

	/// Iterates through all items that are currently presented by the collection view.
	///
	/// Note that, as with `indexPathsForVisibleItems` and `visibleItems`, the
	/// index paths and items returned are currently managed by the collection view
	/// but not necessarily visible.
	public func forEachVisibleItem(_ block: (_ indexPath: IndexPath, _ item: CXCollectionViewItem) -> Void) {
		forEachVisibleIndexPath { indexPath in
			guard let item = self.item(at: indexPath) as? CXCollectionViewItem else {
				assertionFailure("Could not get excavated collection view item at index path '\(indexPath)', either does not exist or is lacking conformance to 'CXCollectionViewItem'.")
				return
			}

			block(indexPath, item)
		}
	}
	
}
