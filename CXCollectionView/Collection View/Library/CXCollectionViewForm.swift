//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 25/10/2020.
//  Copyright © 2020 Apricum Clb. All rights reserved.
//

import Foundation

public protocol CXCollectionViewForm {
	
	/// A data object representation to be used as part of a section in collection view data.
	typealias Object = CXDataObject
	
	/// A data section representation to be used in an ordered collection as collection view data.
	typealias Section = CXDataSection
	
}
