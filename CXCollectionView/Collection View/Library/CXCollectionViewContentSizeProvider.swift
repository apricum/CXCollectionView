//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 24/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// Functionality for calculating required content height, executed on a collection view.
public protocol CXCollectionViewContentSizeProvider {}

extension CXCollectionViewContentSizeProvider {
	
	private typealias References = (collectionView: CXCollectionView, dataSource: CXCollectionViewDataSource, layout: NSCollectionViewLayout, layoutDelegate: NSCollectionViewDelegateFlowLayout)
	
	/// Fallback value used or returned when size is indeterminate.
	///
	/// For properties like `intrinsicContentSize`, AppKit uses `NSView.noIntrinsicMetric`,
	/// a value not intended for calculation that needs to be filtered for. As an indeterminate
	/// size is also a zero size, an actual zero value is used for simplicity.
	static var noEstimatedMetric: CGFloat { 0 }
	
	// MARK: Sizing
	
	/// Calculates and returns the height that is needed for the entire content of the given
	/// collection view to be displayed and visible.
	///
	/// Uses the referenced `dataSource` of the collection view for content information
	/// (e.g. number of sections and number of items in sections) instead of
	/// asking the collection view directly.
	public func estimatedContentHeight(_ collectionView: CXCollectionView) -> CGFloat? {
		guard
			let layout = collectionView.collectionViewLayout,
			let dataSource = collectionView.contentDataSource,
			let layoutDelegate = collectionView.delegate as? NSCollectionViewDelegateFlowLayout
		else {
			return nil
		}
	
		let references: References = (collectionView, dataSource, layout, layoutDelegate)
		return estimatedContentHeight(references)
	}
	
	private func estimatedContentHeight(_ references: References) -> CGFloat? {
		let (collectionView, dataSource, _, _) = references
		let numberOfSections = dataSource.collectionViewNumberOfSections(collectionView)
		
		var contentHeight: CGFloat = 0
		
		for section in 0 ..< numberOfSections {
			let isLastSection = section == numberOfSections - 1
			let sectionHeight = estimatedHeight(references, of: section, isLast: isLastSection)
			
			contentHeight += sectionHeight
		}
		
		return contentHeight
	}
	
	private func estimatedHeight(_ references: References, of section: Int, isLast isLastSection: Bool) -> CGFloat {
		let (collectionView, dataSource, layout, layoutDelegate) = references
		let numberOfItems = dataSource.collectionView(collectionView, sectionAt: section).count
		var sectionHeight = layoutDelegate.collectionView?(collectionView, layout: layout, referenceSizeForHeaderInSection: section).height ?? 0
		let sectionInsets = layoutDelegate.collectionView?(collectionView, layout: layout, insetForSectionAt: section) ?? NSEdgeInsetsZero
		let lineSpace = layoutDelegate.collectionView?(collectionView, layout: layout, minimumLineSpacingForSectionAt: section) ?? 0
		
		if sectionHeight > 0 {
			sectionHeight += sectionInsets.top
		}
		
		if !isLastSection {
			sectionHeight += sectionInsets.bottom
		}
		
		for item in 0 ..< numberOfItems {
			let isLastItem = item == numberOfItems - 1
			let indexPath = IndexPath(item: item, section: section)
			let itemHeight = layoutDelegate.collectionView?(collectionView, layout: layout, sizeForItemAt: indexPath).height ?? 0
			
			sectionHeight += itemHeight
			
			if !isLastItem {
				sectionHeight += lineSpace
			}
		}
		
		return sectionHeight
	}
	
}
