//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 22/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// Functionality for a control object to initiate changes on a collection view.
///
/// Provides functionality to reload difference as an advanced replacement to the built-in `reloadData()`
/// in the form of `reloadChanges(withAnimation:_:)`, supporting full data difference computation and
/// item insert, update and delete animations to transition between data states.
///
/// Works in conjunction with an instance of `CXCollectionScrollView` to adjust content size depending on
/// presented items, ensuring sufficient height for all collection view items to be rendered.
public protocol CXCollectionViewChangeInitiator {
	
	// MARK: Reference Properties
	
	/// The collection view to operate on for difference reloading.
	var changeReceivingCollectionView: CXCollectionView? { get }
	
	/// The enclosing view of the collection view to operate on.
	var changeReceivingCollectionViewEnclosure: CXCollectionEnclosure? { get }
	
	// MARK: Animation Properties
	
	/// The master duration of the animation when reloading changes to the collection.
	var collectionViewAnimationDuration: TimeInterval { get }
	
	// MARK: Lifecycle
	
	/// Lifecycle function called before a change reload is performed.
	///
	/// - Note: Only applies to reloads initiated by protocol; data reloads in the
	/// collection view are always independent.
	func controllerWillReloadChanges(withAnimation shouldAnimate: Bool)
	
	/// Lifecycle function called after a change reload has been performed.
	///
	/// - Note: Only applies to reloads initiated by protocol; data reloads in the
	/// collection view are always independent.
	func controllerDidReloadChanges(withAnimation didAnimate: Bool)
	
}

extension CXCollectionViewChangeInitiator {
	
	public typealias CompletionHandler = () -> Void
	
	// MARK: Properties
	
	public var collectionViewAnimationDuration: TimeInterval { 0.3 }
	
	// MARK: Lifecycle
	
	public func controllerWillReloadChanges(withAnimation shouldAnimate: Bool) {}
	
	public func controllerDidReloadChanges(withAnimation didAnimate: Bool) {}
	
	// MARK: Reloading
	
	/// Reloads all of the data for the collection view by computing difference.
	///
	/// In contrast to the built-in `reloadData` that replaces all internal data
	/// and recreates all items, this function determines the changes made
	/// between the last (original) data and newly sourced data (updated).
	///
	/// This operation either runs with or without animation and calls a
	/// completion handler once all operations (including transitioning and
	/// resizing animations) are finished.
	///
	/// By default, `reloadData` is assumed to be a non-animated reload,
	/// `reloadChanges` is assumed to be an animated reload.
	public func reloadChanges(animate shouldAnimate: Bool = true, _ completionHandler: CompletionHandler? = nil) {
		controllerWillReloadChanges(withAnimation: shouldAnimate)
		
		let deferredCompletionHandler = {
			controllerDidReloadChanges(withAnimation: shouldAnimate)
			completionHandler?()
		}
		
		if shouldAnimate {
			reloadChangesWithAnimation(deferredCompletionHandler)
		} else {
			reloadChangesWithoutAnimation(deferredCompletionHandler)
		}
	}

	// MARK: Execution
	
	/// Executes a collection reload, determines a difference and applies the
	/// changes to the collection view. Animation is skipped, all changes are
	/// visualized as fast as possible without transitions.
	private func reloadChangesWithoutAnimation(_ completionHandler: CompletionHandler? = nil) {
		guard let collectionView = changeReceivingCollectionView else {
			completionHandler?()
			return
		}
		
		let deferredCompletionHandler = {
			completionHandler?()
			collectionView.didUpdateContent(withAnimation: false)
		}
		
		collectionView.prepareContentUpdate()
		collectionView.invalidateCollectionViewHeight()

		guard let collectionViewEnclosure = changeReceivingCollectionViewEnclosure else {
			collectionView.reloadContentIfNeeded()
			deferredCompletionHandler()
			return
		}
		
		collectionViewEnclosure.prepareForEnclosureResize()
		collectionViewEnclosure.resizeEnclosure(withAnimation: false) {
			collectionView.reloadContentIfNeeded()
			deferredCompletionHandler()
		}
	}
	
	/// Executes a collection reload, determines a difference and applies the
	/// changes to the collection view with animations for the transitioning
	/// between the original content state and the new determined one.
	///
	/// The chain of events is outlined below. It should be noted that depending
	/// on the direction in difference of the view height may cause a change in
	/// order. An increase would resize the container first, then update the view
	/// second, a decrease would update first, resize second.
	///
	/// 1. Invoke content update, mark view, use `prepareContentUpdate`.
	/// 2. Determine new fitting collection view height with updated contents.
	/// 3. Trigger animated update to new determined height for the collection view.
	///	4. Invoke "updateContentIfNeeded" to animate collection view to match.
	private func reloadChangesWithAnimation(_ completionHandler: CompletionHandler? = nil) {
		guard let collectionView = changeReceivingCollectionView else {
			return
		}
		
		let deferredCompletionHandler = {
			completionHandler?()
			collectionView.didUpdateContent(withAnimation: true)
		}
		
		collectionView.prepareContentUpdate()
		collectionView.invalidateCollectionViewHeight()
		
		guard let collectionViewEnclosure = changeReceivingCollectionViewEnclosure else {
			collectionView.updateContentIfNeeded {
				deferredCompletionHandler()
			}
			
			return
		}
		
		collectionViewEnclosure.prepareForEnclosureResize()
		
		switch collectionViewEnclosure.preparedHeightResizeDirection {
		case .increasing:
			collectionViewEnclosure.resizeEnclosure(withAnimation: true) {
				collectionView.updateContentIfNeeded {
					deferredCompletionHandler()
				}
			}
		case .decreasing:
			collectionView.updateContentIfNeeded()
			collectionViewEnclosure.resizeEnclosure(withAnimation: true) {
				deferredCompletionHandler()
			}
		case .consistent:
			collectionView.updateContentIfNeeded {
				deferredCompletionHandler()
			}
		}
	}
	
}
