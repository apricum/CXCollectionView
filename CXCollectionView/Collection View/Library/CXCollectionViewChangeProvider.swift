//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 22/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Foundation
import Differ

protocol CXCollectionViewChangeProvider {}

extension CXCollectionViewChangeProvider {
	
	typealias DiffChangeDictionary = [CollectionChangeKind: CollectionDiffSet]
	
	// MARK: Differentiation Merging
	
	func mergedChanges(from diff: NestedExtendedDiff) -> DiffChangeDictionary {
		var mergedChanges: DiffChangeDictionary = [:]
		
		mergedChanges[.deleteSections] = CollectionDiffSet.sectionIndices([])
		mergedChanges[.insertSections] = CollectionDiffSet.sectionIndices([])
		mergedChanges[.moveSections] = CollectionDiffSet.sectionIndexDiffs([])
		mergedChanges[.deleteItems] = CollectionDiffSet.indexPaths([])
		mergedChanges[.insertItems] = CollectionDiffSet.indexPaths([])
		mergedChanges[.moveItems] = CollectionDiffSet.indexPathDiffs([])
		
		for change in diff.elements {
			mergeChange(change, into: &mergedChanges)
		}
		
		return mergedChanges
	}
	
	private func mergeChange(_ change: NestedExtendedDiff.Element, into dictionary: inout DiffChangeDictionary) {
		switch change {
		case .deleteSection(let sectionIndex):
			guard case .sectionIndices(var deletableSectionIndices) = dictionary[.deleteSections]! else { return }
			
			deletableSectionIndices.insert(sectionIndex)
			dictionary[.deleteSections] = .sectionIndices(deletableSectionIndices)
		case .insertSection(let sectionIndex):
			guard case .sectionIndices(var insertableSectionIndices) = dictionary[.insertSections]! else { return }
			
			insertableSectionIndices.insert(sectionIndex)
			dictionary[.deleteSections] = .sectionIndices(insertableSectionIndices)
		case .moveSection(from: let originalSectionIndex, to: let updatedSectionIndex):
			guard case .sectionIndexDiffs(var movableDifferentialSectionIndices) = dictionary[.moveSections]! else { return }
			
			let movableDifferentialSectionIndex = DiffIndex(originalSectionIndex, updatedSectionIndex)
			movableDifferentialSectionIndices.insert(movableDifferentialSectionIndex)
			dictionary[.moveSections] = .sectionIndexDiffs(movableDifferentialSectionIndices)
		case .insertElement(let itemIndex, section: let sectionIndex):
			guard case .indexPaths(var insertableIndexPaths) = dictionary[.insertItems]! else { return }
			
			let insertableIndexPath = IndexPath(item: itemIndex, section: sectionIndex)
			insertableIndexPaths.insert(insertableIndexPath)
			dictionary[.insertItems] = .indexPaths(insertableIndexPaths)
		case .deleteElement(let itemIndex, section: let sectionIndex):
			guard case .indexPaths(var deletableIndexPaths) = dictionary[.deleteItems]! else { return }
			
			let deletableIndexPath = IndexPath(item: itemIndex, section: sectionIndex)
			deletableIndexPaths.insert(deletableIndexPath)
			dictionary[.deleteItems] = .indexPaths(deletableIndexPaths)
		case .moveElement(from: let originalIndexPathValues, to: let updatedIndexPathValues):
			guard case .indexPathDiffs(var movableDifferentialIndexPaths) = dictionary[.moveItems]! else { return }
			
			let originalIndexPath = IndexPath(item: originalIndexPathValues.item, section: originalIndexPathValues.section)
			let updatedIndexPath = IndexPath(item: updatedIndexPathValues.item, section: updatedIndexPathValues.section)
			let movableDifferentialIndexPath = DiffIndexPath(originalIndexPath, updatedIndexPath)
			
			movableDifferentialIndexPaths.insert(movableDifferentialIndexPath)
			dictionary[.moveItems] = .indexPathDiffs(movableDifferentialIndexPaths)
		}
	}
	
}
