//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 11/10/2019.
//  Copyright © 2019 Apricum Clb. All rights reserved.
//

import Cocoa

/// A view that can provide its estimated content size by layout- and data-based calculation.
public protocol CXSizableView: NSView {
	
	/// The estimated size the view requires for all of its content or subviews to be visible.
	var estimatedViewSize: CGSize { get }
	
}
