//
//  Code Excavated Collection View (CXCollectionView)
//
//  Copyright © 2020 August Saint Freytag. All rights reserved.
//

import Foundation

public typealias CXCollectionHeightChange = (original: CGFloat, updated: CGFloat)

public protocol CXCollectionEnclosure: AnyObject {
	
	typealias HeightChange = CXCollectionHeightChange
	typealias ChangeDirection = CXCollectionChangeDirection
	
	typealias CompletionHandler = () -> Void
	
	// MARK: Properties
	
	/// The prepared and cached difference in estimated height for the
	/// enclosed view before being applied in a scheduled layout pass.
	var preparedEnclosedViewHeightChange: HeightChange? { get set }
	
	/// The estimated or calculated height of the enclosed view
	/// given its currently available or sourcable content.
	var enclosedViewHeight: CGFloat { get }
	
	/// The height defined through active layout constraints set
	/// around the enclosed view. Used to determine size difference.
	var enclosedViewAppliedHeight: CGFloat { get }
	
	// MARK: Operations
	
	func prepareForEnclosureResize()
	
	func resizeEnclosure(withAnimation shouldAnimate: Bool)
	
	func resizeEnclosure(withAnimation shouldAnimate: Bool, _ completionHandler: CompletionHandler?)
	
}

extension CXCollectionEnclosure {
	
	// MARK: Layout Lifecycle
	
	/// Called to prepare for a scheduled resize, note current height
	/// and target height for enclosure resize.
	public func prepareForEnclosureResize() {
		let previouslyAppliedHeight = enclosedViewAppliedHeight
		preparedEnclosedViewHeightChange = (previouslyAppliedHeight, enclosedViewHeight)
	}
	
	// MARK: Difference Form
	
	public var preparedHeightResizeDirection: ChangeDirection {
		guard let heightChange = preparedEnclosedViewHeightChange else {
			return .consistent
		}
		
		if heightChange.updated > heightChange.original {
			return .increasing
		} else if heightChange.updated < heightChange.original {
			return .decreasing
		} else {
			return .consistent
		}
	}
	
	public var needsEnclosureResize: Bool {
		guard let heightChange = preparedEnclosedViewHeightChange else {
			return false
		}
		
		return heightChange.original != heightChange.updated
	}
	
	// MARK: Convenience
	
	public func resizeEnclosure(withAnimation shouldAnimate: Bool) {
		resizeEnclosure(withAnimation: shouldAnimate, nil)
	}
	
}
