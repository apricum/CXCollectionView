//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 16/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Cocoa

/// A set of methods that an item forming object implements to provide collection view
/// item set-up and possibly visualization capabilities.
public protocol CXCollectionViewItemForm: AnyObject, CXCollectionViewForm {
	
	func registerCollectionViewItems(_ collectionView: CXCollectionView)
	
	func collectionView(_ collectionView: CXCollectionView, object: Object, at indexPath: IndexPath) -> NSCollectionViewItem
	
	func collectionView(_ collectionView: CXCollectionView, viewForSupplementaryElementOfKind kind: NSCollectionView.SupplementaryElementKind, at indexPath: IndexPath) -> NSView
	
}

public extension CXCollectionViewItemForm {
	
	func collectionView(_ collectionView: CXCollectionView, viewForSupplementaryElementOfKind kind: NSCollectionView.SupplementaryElementKind, at indexPath: IndexPath) -> NSView {
		assertionFailure("Requested supplementary view of kind '\(kind)' but item form is not set up to supply such a view.")
		return NSView()
	}
	
}
