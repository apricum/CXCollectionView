//
//  Code Excavated Collection View (CXCollectionView)
//
//  Copyright © 2020 August Saint Freytag. All rights reserved.
//

import Foundation

public enum CXCollectionChangeDirection {
	
	case increasing
	case decreasing
	case consistent
	
}
