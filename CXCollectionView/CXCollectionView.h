//
//  CXCollectionView.h
//  CXCollectionView
//
//  Created by August Saint Freytag on 25/08/2019.
//  Copyright © 2019 Apricum Clb. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for CXCollectionView.
FOUNDATION_EXPORT double CXCollectionViewVersionNumber;

//! Project version string for CXCollectionView.
FOUNDATION_EXPORT const unsigned char CXCollectionViewVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CXCollectionView/PublicHeader.h>


