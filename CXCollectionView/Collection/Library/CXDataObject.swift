//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 11/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Foundation

/// Protocol describing a modeled item that can appear in a modeled section as part of a collection view.
public protocol CXDataObject {
	
	/// The identifier type used with modeled objects, commonly a precomputed hash value.
	typealias Identifier = Int
	
	/// The unique identifier of the modeled object.
	var objectId: Identifier { get }
	
}
