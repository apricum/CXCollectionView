//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 18/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Foundation

/// Model describing a section of objects that can be used as data in a collection view.
public struct CXDataSection {
	
	/// The identifier type used with modeled sections, commonly a precomputed hash value.
	public typealias Identifier = Int
	public typealias ObjectCollection = [CXDataObject]
	
	/// The unique identifier of the modeled section.
	public let sectionId: Identifier
	
	/// The objects stored inside the modeled section.
	public var objects: [CXDataObject]
	
	// MARK: Init
	
	/// Create a new data section without objects and a unique identifier set on `init`.
	public init() {
		self.init(id: UUID().hashValue, [])
	}
	
	/// Create a new data section with the given objects and a unique identifier set on `init`.
	public init(_ objects: ObjectCollection) {
		self.init(id: UUID().hashValue, objects)
	}
	
	/// Create a new data section with the given identifier and objects.
	public init(id: Identifier, _ objects: ObjectCollection) {
		self.sectionId = id
		self.objects = objects
	}
	
}

// MARK: Content Equation

extension CXDataSection {
	
	/// Checks if the section has equal objects as the supplied section.
	public func hasEqualCollectionObjects(as section: CXDataSection) -> Bool {
		guard count == section.count else {
			return false
		}
		
		return objects.map { $0.objectId } == section.objects.map { $0.objectId }
	}
	
}

// MARK: Collection

extension CXDataSection: Collection {
	
	public typealias Index = ObjectCollection.Index
	public typealias Element = ObjectCollection.Element
	
	public var startIndex: CXDataSection.ObjectCollection.Index { return objects.startIndex }
	public var endIndex: CXDataSection.ObjectCollection.Index { return objects.endIndex }
	
	public subscript(index: Index) -> Element {
		get { return objects[index] }
	}
	
	public func index(after i: CXDataSection.ObjectCollection.Index) -> CXDataSection.ObjectCollection.Index {
		return objects.index(after: i)
	}
	
}
