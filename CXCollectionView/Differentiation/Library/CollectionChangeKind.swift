//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 22/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Foundation

enum CollectionChangeKind: Hashable {
	
	case insertSections
	case deleteSections
	case moveSections
	case insertItems
	case deleteItems
	case moveItems
	
}
