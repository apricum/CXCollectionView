//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 22/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Foundation

typealias DiffIndex = CollectionItemDiff<Int>
typealias DiffIndexPath = CollectionItemDiff<IndexPath>

enum CollectionDiffSet {
	
	case sectionIndices(Set<Int>)
	case sectionIndexDiffs(Set<DiffIndex>)
	case indexPaths(Set<IndexPath>)
	case indexPathDiffs(Set<DiffIndexPath>)
	
}
