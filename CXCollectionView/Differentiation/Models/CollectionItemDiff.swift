//
//  Code Excavated Collection View (CXCollectionView)
//
//  Created by August Saint Freytag on 22/08/2019.
//  Copyright © 2019 August Saint Freytag. All rights reserved.
//

import Foundation

struct CollectionItemDiff<Item: Hashable>: Hashable {
	
	let original: Item
	let updated: Item
	
	init(_ original: Item, _ updated: Item) {
		self.original = original
		self.updated = updated
	}
	
}
